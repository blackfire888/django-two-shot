from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountsForm


# Create your views here.


@login_required
def receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list_receipts": receipts,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt=form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")   
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "list_categories": categories,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def accounts_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "list_accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense=form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")   
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountsForm(request.POST)
        if form.is_valid():
            account=form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts_list")   
    else:
        form = AccountsForm()
    context = {"form": form}
    return render(request, "receipts/accounts/create.html", context)